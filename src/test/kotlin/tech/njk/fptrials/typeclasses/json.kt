package tech.njk.fptrials.typeclasses.json

import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should be`
import org.amshove.kluent.`should equal`
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import org.junit.jupiter.api.Assertions.assertEquals

object JsonMarshallersTest : Spek(
    {
      describe("From the marshaller instances") {
        it("a product marshaller should marshall an product to a JSON value") {
          val product = Model.Product("product", "description")
          val result = MarshallerInstances.productMarshaller.run {
            product.toJson().toString()
          }
          result `should be equal to` """{"productId":"product","description":"description"}"""
        }
        it("a orderline marshaller should marshall an order line to a JSON value") {
          val orderLine = Model.OrderLine(1, 2, Model.Product("", ""))
          val result = MarshallerInstances.orderLineMarshaller.run {
            orderLine.toJson().toString()
          }
          result `should be equal to` """{"lineId":1,"quantity":2,"product":{"productId":"","description":""}}"""
        }
        it("a order marshaller should marshall a order to a JSON value") {
          val orderLines = listOf(
              Model.OrderLine(1, 2, Model.Product("A1", "A1")),
              Model.OrderLine(2, 3, Model.Product("A2", "A2")))
          val order = Model.Order("orderId", orderLines)
          val result = MarshallerInstances.orderMarshaller.run {
            order.toJson().toString()
          }
          assertEquals(result, """{"orderId":"orderId","customorderlines":[{"lineId":1,"quantity":2,"product":{"productId":"A1","description":"A1"}},{"lineId":2,"quantity":3,"product":{"productId":"A2","description":"A2"}}]}""")
//          result `should be equal to` """{"orderId":"orderId","customorderlines":[{"lineId":1,"quantity":2,"product":{"productId":"A1","description":"A1"}},{"lineId":2,"quantity":3,"product":{"productId":"A2","description":"A2"}}]}"""
        }
      }
    }
)

object UnmarshallersSpecTest: Spek({
  describe("From the unmarshaller instances") {
    it("product unmarshaller should unmarshall a product JSON value to product object") {
      val productJson = """{"productId":"product","description":"description"}"""
      val product = Model.Product("product", "description")
      val result = UnmarshallerInstances.productUnmarshaller.run {
        fromJson(productJson)
      }
      result.isRight() `should be` true
      result.map {
        it `should equal` product
      }
    }

    it("product unmarshaller should return a Left when JSON is invalid") {
      val result = UnmarshallerInstances.productUnmarshaller.run {
        fromJson("invalid")
      }
      result.isLeft() `should be` true
    }

    it("orderline unmarshaller should unmarshall an orderline JSON value to orderline object") {
      val orderLine = Model.OrderLine(1, 2, Model.Product("", ""))
      val orderLineJson = """{"lineId":1,"quantity":2,"product":{"productId":"","description":""}}"""
      val result = UnmarshallerInstances.orderLineUnmarshaller.run {
        fromJson(orderLineJson)
      }
      result.isRight() `should be` true
      result.map {
        it `should equal` orderLine
      }
    }

    it("orderline unmarshaller should return a Left when JSON is invalid") {
      val result = UnmarshallerInstances.orderLineUnmarshaller.run {
        fromJson("invalid")
      }
      result.isLeft() `should be` true
    }

    it("Order unmarshaller should unmarshall an order JSON value to order object") {
      val orderLines = listOf(Model.OrderLine(1, 2, Model.Product("", "")), Model.OrderLine(2, 3, Model.Product("", "")))
      val order = Model.Order("orderId", orderLines)
      val orderJson = """{"orderId":"orderId","customorderlines":[{"lineId":1,"quantity":2,"product":{"productId":"","description":""}},{"lineId":2,"quantity":3,"product":{"productId":"","description":""}}]}"""
      val result = UnmarshallerInstances.orderUnmarshaller.run {
        fromJson(orderJson)
      }
      result.isRight() `should be` true
      result.map {
        it `should equal` order
      }
    }

    it("order unmarshaller should return a Left when JSON is invalid") {
      val result = UnmarshallerInstances.orderUnmarshaller.run {
        fromJson("invalid")
      }
      result.isLeft() `should be` true
    }
  }

})


