package tech.njk.fptrials.coroutines

import kotlinx.coroutines.*

fun main() = runBlocking<Unit> { // start main coroutine
  launch { // launch a new coroutine in background and continue
    doWorld()
  }
  println("Hello,") // main coroutine continues here immediately
}

private suspend fun doWorld() {
  delay(5000L)
  println("World!")
}