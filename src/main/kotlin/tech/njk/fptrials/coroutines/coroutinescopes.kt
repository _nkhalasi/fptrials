package tech.njk.fptrials.coroutines

import kotlinx.coroutines.*

object MyException1 : Throwable("Dummy exception", null, false, false)

suspend fun main() {

  val scope = CoroutineScope(CoroutineName("Parent"))
  // New job gets created if not provided explicitely
  if (scope.coroutineContext[Job] != null) {
    println("New job is created!")
  }

  scope.launch {
    launch(CoroutineName("Child-A")) {
      delay(100)
      println("${Thread.currentThread().name} throwing exception")
      throw MyException1
    }

    launch(CoroutineName("Child-B")) {
      println("${Thread.currentThread().name} before exception...")
      delay(500)
      println("${Thread.currentThread().name} after exception...")
    }
  }

  delay(1000)
}