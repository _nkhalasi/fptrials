package tech.njk.fptrials.coroutines.trials01

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*

fun log(v: Any) = println("[${Thread.currentThread().name}] $v")

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
private suspend fun performSquaring(numbersChannel: ReceiveChannel<Int>) {
  numbersChannel.consumeEach {
    coroutineScope {
      val result = it * it
      log("$it squared is ${result}")
    }
  }
}

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
fun main() = runBlocking {
  val numbersChannel = produce(CoroutineName("number-generator") + Dispatchers.Default) {
    var x = 1
    while (true) {
      delay(1000)
      send(x++)
      log("Requested square of $x")
    }
  }
  coroutineScope {
    launch(CoroutineName("number-square-worker-1") + Dispatchers.IO) { performSquaring(numbersChannel) }
    launch(CoroutineName("number-square-worker-2") + Dispatchers.IO) { performSquaring(numbersChannel) }
    launch(CoroutineName("number-square-worker-3") + Dispatchers.IO) { performSquaring(numbersChannel) }
    launch(CoroutineName("number-square-worker-4") + Dispatchers.IO) { performSquaring(numbersChannel) }
    launch(CoroutineName("number-square-worker-5") + Dispatchers.IO) { performSquaring(numbersChannel) }
  }
  coroutineContext.cancelChildren()
}