package tech.njk.fptrials.coroutines

import kotlinx.coroutines.*

fun main() = runBlocking<Unit> { // start main coroutine
  val job = GlobalScope.launch { // launch a new coroutine in background and continue
    delay(5000L)
    println("World!")
  }
  println("Hello,") // main coroutine continues here immediately
  delay(3000L)      // delaying for 2 seconds to keep JVM alive
  println("Now waiting for $job")
  job.join()
}
