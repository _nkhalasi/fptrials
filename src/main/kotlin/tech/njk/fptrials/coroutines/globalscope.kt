package tech.njk.fptrials.coroutines

import kotlinx.coroutines.*

object MyException : Throwable("Dummy exception", null, false, false)

fun main() {

  runBlocking {
    println("My context is $coroutineContext")
    println("My context is $coroutineContext[Job]")
    println("My context is ${coroutineContext + CoroutineName("test")}")
  }

  val job = GlobalScope.coroutineContext[Job]
  if (job == null) {
    println("No Job associated with GlobalScope!")
  }

  // exception thrown in this coroutine does not affect other coroutines
  GlobalScope.launch {
    delay(500)
    println("throwing exception")
    throw MyException
  }

  GlobalScope.launch {
    println("before exception...")
    delay(1000)
    println("after exception...")
  }

  Thread.sleep(1500)
}