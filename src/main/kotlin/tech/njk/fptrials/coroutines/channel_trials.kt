package tech.njk.fptrials.coroutines

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import kotlinx.coroutines.selects.select
import org.slf4j.LoggerFactory
import tech.njk.fptrials.inline.logger
import kotlin.coroutines.CoroutineContext

val LOG = LoggerFactory.getLogger("Number-Square-Machine")

class SquareNumbersMachine(scope: CoroutineScope) : CoroutineScope {
  companion object {
    val log = logger<SquareNumbersMachine>()
  }

  private val job = Job(scope.coroutineContext[Job])
  override val coroutineContext: CoroutineContext
    get() = job + CoroutineName("number-square") + Dispatchers.Unconfined

  private data class NumberSquareRequest(
      val deferred: CompletableDeferred<Int>,
      val num: Int
  )

  @ObsoleteCoroutinesApi
  @ExperimentalCoroutinesApi
  private val squareWorker: SendChannel<NumberSquareRequest> = actor {
    consumeEach { request ->
      val squaredNum = squareTheNumber(request.num)
      request.deferred.complete(squaredNum)
    }
  }

  private fun squareTheNumber(num: Int): Int {
    val squared = num * num
    log.debug("Square of $num is $squared")
    return squared
  }

  @ExperimentalCoroutinesApi
  @ObsoleteCoroutinesApi
  suspend fun submitRequest(num: Int): Int {
    val request = NumberSquareRequest(CompletableDeferred(), num)

    return select {
      squareWorker.onSend(request) {
        request.deferred.await()
      }
    }
  }

  fun destroy() = coroutineContext.cancel()

}

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
private suspend fun performSquaring(numbersChannel: ReceiveChannel<Int>, squaringMachine: SquareNumbersMachine) {
  numbersChannel.consumeEach {
    coroutineScope {
      val result = squaringMachine.submitRequest(it)
      LOG.debug("$it squared is ${result}")
    }
  }
}

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
fun main() = runBlocking {
  val squaringMachine = SquareNumbersMachine(this)
  val numbersChannel = produce(CoroutineName("number-generator") + Dispatchers.IO) {
    var x = 1
    while (true) {
      delay(1000)
      send(x++)
    }
  }
  coroutineScope {
    launch(CoroutineName("number-square-worker-1") + Dispatchers.IO) { performSquaring(numbersChannel, squaringMachine) }
    launch(CoroutineName("number-square-worker-2") + Dispatchers.IO) { performSquaring(numbersChannel, squaringMachine) }
    launch(CoroutineName("number-square-worker-3") + Dispatchers.IO) { performSquaring(numbersChannel, squaringMachine) }
    launch(CoroutineName("number-square-worker-4") + Dispatchers.IO) { performSquaring(numbersChannel, squaringMachine) }
    launch(CoroutineName("number-square-worker-5") + Dispatchers.IO) { performSquaring(numbersChannel, squaringMachine) }
  }

  delay(1000)
  squaringMachine.destroy()
}