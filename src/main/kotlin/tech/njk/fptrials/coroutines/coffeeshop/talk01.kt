package tech.njk.fptrials.coroutines.coffeeshop
import java.lang.Thread.sleep
import kotlin.system.measureTimeMillis

fun main() {
    val orders = listOf(
        Menu.Cappuccino(CoffeeBean.Regular, Milk.Whole),
        Menu.Cappuccino(CoffeeBean.Premium, Milk.Breve),
        Menu.Cappuccino(CoffeeBean.Regular, Milk.NonFat),
        Menu.Cappuccino(CoffeeBean.Decaf, Milk.Whole),
        Menu.Cappuccino(CoffeeBean.Regular, Milk.NonFat),
        Menu.Cappuccino(CoffeeBean.Decaf, Milk.NonFat)
    )

    orders.forEach { log(it) }

    val time = measureTimeMillis {
        orders.forEach {
            val groundBeans = grindCoffeeBeans(it.beans)
            val espresso = pullEspressoShot(groundBeans)
            val steamedMilk = steamMilk(it.milk)
            val cappuccino = makeCappuccino(it, espresso, steamedMilk)
          log("serve: $cappuccino")
        }
    }
  log("time: $time ms")
}


private fun grindCoffeeBeans(beans: CoffeeBean): CoffeeBean.GroundBeans {
  log("grinding coffee beans")
    sleep(100)
    return CoffeeBean.GroundBeans(beans)
}

private fun pullEspressoShot(groundBeans: CoffeeBean.GroundBeans): Espresso {
  log("pulling espresso shot")
    sleep(60)
    return Espresso(groundBeans)
}

private fun steamMilk(milk: Milk): Milk.SteamedMilk {
  log("steaming milk")
    sleep(30)
    return Milk.SteamedMilk(milk)
}

private fun makeCappuccino(
    order: Menu.Cappuccino,
    espresso: Espresso,
    steamedMilk: Milk.SteamedMilk
): Beverage.Cappuccino {
  log("making cappuccino")
    sleep(10)
    return Beverage.Cappuccino(order, espresso, steamedMilk)
}
