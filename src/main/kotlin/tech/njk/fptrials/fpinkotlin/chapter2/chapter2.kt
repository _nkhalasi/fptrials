package tech.njk.fptrials.fpinkotlin.chapter2

import kotlin.collections.List

object Example {
  fun abs(n: Int): Int = if (n < 0) -n else n
  fun factorial(i: Int): Int {
    tailrec fun go(n: Int, acc: Int): Int = if (n <= 0) acc else go(n-1, acc * n)
    return go(i, 1)
  }
}

fun formatResult(name: String, n: Int, f: (Int) -> Int): String =
    "The %s of %d is %d.".format(name, n, f(n))

fun <A> findFirst(xs: Array<A>, p: (A) -> Boolean): Int {
  tailrec fun loop(n: Int): Int =
      when {
        n >= xs.size -> -1
        p(xs[n]) -> n
        else -> loop(n+1)
      }
  return loop(0)
}

val <T> List<T>.tail: List<T>
  get() = drop(1)
val <T> List<T>.head: T
  get() = first()

fun <A> isSorted(aa: List<A>, ordered: (A, A) -> Boolean): Boolean {
  tailrec fun loop(first: A, remaining: List<A>): Boolean {
    val second = remaining.head
    val rest = remaining.tail
    return if (ordered(first, second)) {
      if (rest.isNotEmpty())
        loop(second, rest)
      else true
    } else false
  }
  return when (aa.size) {
    0, 1 -> true
    else -> loop(aa.head, aa.tail)
  }
}

fun <A, B, C> partial1(a: A, f: (A, B) -> C): (B) -> C = { b: B -> f(a, b) }
fun <A, B, C> curry(f: (A, B) -> C): (A) -> (B) -> C = { a: A -> { b: B -> f(a, b) } }
fun <A, B, C> uncurry(f: (A) -> (B) -> C): (A, B) -> C = { a: A, b: B -> f(a)(b) }
fun <A, B, C> compose(f: (B) -> C, g: (A) -> B): (A) -> C = { a: A -> f(g(a)) }

fun main() {
  println(formatResult("factorial", 7, Example::factorial))
  println(formatResult("absolute value", -42, Example::abs))
  println(
      formatResult("absolute value", -101) {
        if (it < 0) -it else it
      }
  )

  println(isSorted(listOf<Int>()) { f: Int, s: Int -> f >= s })
  println(isSorted(listOf(1)) { f: Int, s: Int -> f >= s })
  println(isSorted(listOf(1, 1)) { f: Int, s: Int -> f <= s })
  println(isSorted(listOf(1, 2)) { f: Int, s: Int -> f <= s })
  println(isSorted(listOf(1, 2, 3)) { f: Int, s: Int -> f <= s })
  println(isSorted(listOf(1, 3, 2)) { f: Int, s: Int -> f <= s })
  println(isSorted(listOf(1, 3, 5, 7)) { f: Int, s: Int -> f <= s })
  println(isSorted(listOf(1, 3, 5, 7, 1)) { f: Int, s: Int -> f <= s })
}
