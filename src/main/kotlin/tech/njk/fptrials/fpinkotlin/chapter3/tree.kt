package tech.njk.fptrials.fpinkotlin.chapter3


sealed class Tree<out A>
data class Leaf<A>(val value: A) : Tree<A>()
data class Branch<A>(val left: Tree<A>, val right: Tree<A>) : Tree<A>()

fun <A> size(tree: Tree<A>): Int =
    when (tree) {
      is Leaf -> 1
      is Branch -> 1 + size(tree.left) + size(tree.right)
    }

fun maximum(tree: Tree<Int>): Int =
    when (tree) {
      is Leaf -> tree.value
      is Branch -> maxOf(maximum(tree.left), maximum(tree.right))
    }

fun <A> depth(tree: Tree<A>): Int =
    when (tree) {
      is Leaf -> 0
      is Branch -> 1 + maxOf(depth(tree.left), depth(tree.right))
    }

fun <A, B> map(tree: Tree<A>, f: (A) -> B): Tree<B> =
    when (tree) {
      is Leaf -> Leaf(f(tree.value))
      is Branch -> Branch(map(tree.left, f), map(tree.right, f))
    }

fun <A, B> fold(ta: Tree<A>, lf: (A) -> B, bf: (B, B) -> B): B =
    when (ta) {
      is Leaf -> lf(ta.value)
      is Branch -> bf(fold(ta.left, lf, bf), fold(ta.right, lf, bf))
    }

fun <A> sizeF(ta: Tree<A>): Int =
    fold(ta, { 1 }, { b1, b2 -> 1 + b1 + b2 })

fun maximumF(ta: Tree<Int>): Int =
    fold(ta, { a -> a }, { b1, b2 -> maxOf(b1, b2) })

fun <A> depthF(ta: Tree<A>): Int =
    fold(ta, { 0 }, { b1, b2 -> 1 + maxOf(b1, b2) })

fun <A, B> mapF(ta: Tree<A>, f: (A) -> B): Tree<B> =
    fold(ta, { a: A -> Leaf(f(a)) },
        { b1: Tree<B>, b2: Tree<B> -> Branch(b1, b2) })

fun main() {
  val family: Tree<String> = Branch(
      Branch(
          Branch(Leaf("Naresh"), Leaf("Yogen")),
          Leaf("Chaganbhai")),
      Leaf("Dhaniben")
  )
  println(family)
  println("Size: ${sizeF(family)}")
  println("Depth: ${depthF(family)}")
  println("MapF: ${mapF(family, { name -> "$name Chapparia"})}")
}
