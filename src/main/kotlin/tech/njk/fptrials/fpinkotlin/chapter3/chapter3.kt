package tech.njk.fptrials.fpinkotlin.chapter3

import java.lang.IllegalStateException

sealed class List<out A> {
  companion object {
    fun <A> of(vararg aa: A): List<A> {
      val tail = aa.sliceArray(1 until aa.size)
      return if (aa.isEmpty()) Nil else Cons(aa[0], of(*tail))
    }

    fun sum(ints: List<Int>): Int =
        when (ints) {
          is Nil -> 0
          is Cons -> ints.head + sum(ints.tail)
        }

    fun product(doubles: List<Double>): Double =
        when (doubles) {
          is Nil -> 1.0
          is Cons -> {
            if (doubles.head == 0.0) 0.0
            else doubles.head * product(doubles.tail)
          }
        }

    fun <A> empty(): List<A> = Nil
  }
}

object Nil: List<Nothing>()
data class Cons<out A>(val head: A, val tail: List<A>): List<A>()

val ex1: List<Double> = Nil
val ex2: List<Int> = Cons(1, Nil)
val ex3: List<String> = Cons("a", Cons("b", Nil))
val ex4 = List.of("a", "b")


fun <A> tail(xs: List<A>): List<A> =
    when (xs) {
      is Nil -> throw IllegalStateException("Nil cannot have a `tail`")
      is Cons -> xs.tail
    }

fun <A> setHead(xs: List<A>, x: A): List<A> =
    when(xs) {
      is Nil -> throw IllegalStateException("Cannot replace `head` of a Nil list")
      is Cons -> Cons(x, tail(xs))
    }


fun <A> drop(l: List<A>, n: Int): List<A> {
  tailrec fun loop(xs: List<A>, c: Int): List<A> =
      when (xs) {
        is Nil -> Nil
        is Cons -> {
          if (c < n) loop(xs.tail, c+1) else xs
        }
      }

  return loop(l, 0)
}

fun <A> dropWhile(l: List<A>, f: (A) -> Boolean): List<A> {
  tailrec fun loop(xs: List<A>): List<A> =
      when (xs) {
        is Nil -> Nil
        is Cons -> {
          if (f(xs.head)) loop(xs.tail) else xs
        }
      }

  return loop(l)
}

fun <A> init(l: List<A>): List<A> =
    when (l) {
      is Cons -> if (l.tail == Nil) Nil else Cons(l.head, init(l.tail))
      is Nil -> throw IllegalStateException("Cannot `init` Nil list")
    }

//Not tail recursive
fun <A> append(a1: List<A>, a2: List<A>): List<A> =
    when (a1) {
      is Nil -> a2
      is Cons -> Cons(a1.head, append(a1.tail, a2))
    }

//Not tail recursive and will result in a StackOverflowError for large lists
fun <A, B> foldRight(xs: List<A>, z: B, f: (A, B) -> B): B =
    when (xs) {
      is Nil -> z
      is Cons -> f(xs.head, foldRight(xs.tail, z, f))
    }

fun sum2(ints: List<Int>): Int = foldRight(ints, 0, { a, b -> a + b })
fun product2(dbs: List<Double>): Double = foldRight(dbs, 1.0, { a, b -> a * b })

fun <A> length(xs: List<A>): Int =
    foldRight(xs, 0, { _, acc -> 1 + acc })

tailrec fun <A, B> foldLeft(xs: List<A>, z: B, f: (B, A) -> B): B =
    when (xs) {
      is Nil -> z
      is Cons -> foldLeft(xs.tail, f(z, xs.head), f)
    }

fun sumL(xs: List<Int>): Int = foldLeft(xs, 0, { x, y -> x + y })
fun productL(xs: List<Double>): Double = foldLeft(xs, 1.0, { x, y -> x * y })
fun <A> lengthL(xs: List<A>): Int = foldLeft(xs, 0, { acc, _ -> acc + 1 })
fun <A> reverse(xs: List<A>): List<A> = foldLeft(xs, List.empty(), { t: List<A>, h: A -> Cons(h, t) })
//foldRight implementation using foldLeft to prevent large lists from blowing the stack
fun <A, B> foldRightL(xs: List<A>, z: B, f: (A, B) -> B): B =
    foldLeft(xs, { b: B -> b }, { g, a -> { b -> g(f(a, b)) } })(z)
fun <A, B> mapL(xs: List<A>, f: (A) -> B): List<B> = foldRightL(xs, List.empty(), { x, acc -> Cons(f(x), acc) })
//Uses the foldRight variant that uses foldLeft in order to prevent large lists from blowing the stack.
fun <A> filter(xs: List<A>, f: (A) -> Boolean): List<A> =
    foldRightL(xs, List.empty(), { a, ls -> if (f(a)) Cons(a, ls) else ls })

fun <A, B> List<A>.foldRightExt(z: B, f: (A, B) -> B): B = foldRightL(this, z, f)
fun <A, B> List<A>.foldLeftExt(z: B, f: (B, A) -> B): B = foldLeft(this, z, f)

fun main() {
  println(drop(Nil, 2))
  println(drop(List.of(1, 2, 3), 0))
  println(drop(List.of(1, 2, 3), 1))
  println(drop(List.of(1, 2, 3), 2))
  println(drop(List.of(1, 2, 3), 3))
  println(drop(List.of(1, 2, 3), 4))

  println("--**--".repeat(5))
  println(dropWhile(Nil) { i: Int -> i % 2 == 0 })
  println(dropWhile(List.of(1, 3, 5)) { i: Int -> i % 2 == 0 })
  println(dropWhile(List.of(2, 4, 6)) { i: Int -> i % 2 == 0 })
  println(dropWhile(List.of(2, 4, 6, 1, 3, 5)) { i: Int -> i % 2 == 0 })
  println(dropWhile(List.of(1, 2, 3, 4, 5, 6)) { i: Int -> i % 2 == 0 })

  println("--**--".repeat(5))
  println(append(Nil, Nil))
  println(append(List.of(1, 2), Nil))
  println(append(Nil, List.of(3, 4)))
  println(append(List.of(1, 2), List.of(3, 4)))

  println("--**--".repeat(5))
  println(sum2(Nil))
  println(sum2(List.of(10)))
  println(sum2(List.of(1, 2, 3, 4)))
  println(product2(Nil))
  println(product2(List.of(10.0)))
  println(product2(List.of(1.0, 2.0, 3.0, 4.0)))

  println("--**--".repeat(5))
  println(foldRight(List.of(1, 2, 3), List.empty<Int>(), { x, y -> Cons(x * 2, y) }))
  println(foldRight(List.of(1, 2, 3), Nil as List<Int>, { x, y -> Cons(x + 2, y) }))

  println("--**--".repeat(5))
  println(length(Nil))
  println(length(List.of("Naresh", "Khalasi")))
  println(length(List.of(1)))
  println(length(List.of(1, 2)))
  println(length(List.of(1, 2, 3)))
  println(length(List.of(1, 2, 3, 4)))

  println("--**--".repeat(5))
  val f = { x: Int, y: List<Int> -> Cons(x, y) }
  val z = Nil as List<Int>

  //See what happens when you pass Nil and Cons themselves to foldRight, like this:
  val step1 = foldRight(List.of(1, 2, 3), z, f)
  val step2 = Cons(1, foldRight(List.of(2, 3), z, f))
  val step3 = Cons(1, Cons(2, foldRight(List.of(3), z, f)))
  val step4 = Cons(1, Cons(2, Cons(3, foldRight(List.empty(), z, f))))
  val step5: List<Int> = Cons(1, Cons(2, Cons(3, Nil)))
  //Replacing z and f with Nil and Cons respectively when invoking foldRight results in xs being copied.

  println("--**--".repeat(5))
  println(mapL(List.of(1, 2, 3)) { x -> x + 1 })

  println("--**--".repeat(5))
  println(filter(Nil as List<Int>) { x: Int -> x % 2 == 0 })
  println(filter(List.of(1, 2, 3, 4, 5)) { x: Int -> x % 2 == 0 })
}