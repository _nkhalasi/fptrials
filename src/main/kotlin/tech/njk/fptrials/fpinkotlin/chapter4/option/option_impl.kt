package tech.njk.fptrials.fpinkotlin.chapter4.option

import tech.njk.fptrials.fpinkotlin.chapter3.Cons
import tech.njk.fptrials.fpinkotlin.chapter3.List
import tech.njk.fptrials.fpinkotlin.chapter3.Nil
import tech.njk.fptrials.fpinkotlin.chapter3.foldRightExt
import kotlin.math.pow


sealed class Option<out A>

data class Some<out A>(val value: A): Option<A>()
object None: Option<Nothing>()

fun <A, B> Option<A>.map(f: (A) -> B): Option<B> =
    when (this@map) {
      is None -> None
      is Some -> Some(f(value))
    }

fun <A> Option<A>.getOrElse(default: () -> A): A =
    when (this@getOrElse) {
      is None -> default()
      is Some -> value
    }

fun <A, B> Option<A>.flatMap(f: (A) -> Option<B>): Option<B> =
    this.map(f).getOrElse { None }

fun <A> Option<A>.orElse(ob: () -> Option<A>): Option<A> =
    this.map { Some(it) }.getOrElse { ob() }

fun <A> Option<A>.filter(f: (A) -> Boolean): Option<A> =
    this.flatMap { a -> if (f(a)) Some(a) else None }

//-----------------------
data class Employee(
    val name: String,
    val department: String,
    val manager: Option<String>)
fun lookupByName(name: String): Option<Employee> = None //TODO()
val timDepartment: Option<String> = lookupByName("Tim").map { it.department }
val timManager: String = lookupByName("Tim")
    .flatMap { it.manager }
    .getOrElse { "Unemployed" }

//-----------------------
fun mean(xs: kotlin.collections.List<Double>): Option<Double> =
    if (xs.isEmpty()) None
    else Some(xs.sum()/xs.size)

fun variance(xs: kotlin.collections.List<Double>): Option<Double> =
  mean(xs).flatMap { m ->
    mean(xs.map { x -> (x - m).pow(2) })
  }
//-----------------------

fun <A, B> lift(f: (A) -> B): (Option<A>) -> Option<B> = { oa -> oa.map(f) }

val abs0: (Option<Double>) -> Option<Double> = lift { kotlin.math.abs(it) }

fun <A> TryToOption(a: () -> A): Option<A> =
    try {
      Some(a())
    } catch(e: Throwable) {
      None
    }

fun <A, B, C> map2(oa: Option<A>, ob: Option<B>, f: (A, B) -> C): Option<C> =
    oa.flatMap { a -> ob.map {  b -> f(a, b) } }

/**
 * Top secret formula for computing an annual car
 * insurance premium from two key factors.
 */
fun insuranceRateQuote(age: Int, numberOfSpeedingTickets: Int): Double = TODO()
fun parseInsuranceQuote(age: String, speedingTickets: String): Option<Double> {
  val optAge: Option<Int> = TryToOption { age.toInt() }
  val optTickets: Option<Int> = TryToOption { speedingTickets.toInt() }
  //return insuranceRateQuote(optAge, optTickets)
  return map2(optAge, optTickets) { a, t ->
    insuranceRateQuote(a, t)
  }
}
//-----------------------

fun <A> sequence(xs: List<Option<A>>): Option<List<A>> =
    xs.foldRightExt(Some(Nil), { oa1: Option<A>, oa2: Option<List<A>> -> map2(oa1, oa2) { a1: A, a2: List<A> -> Cons(a1, a2) } })

fun <A, B> traverse(xa: List<A>, f: (A) -> Option<B>): Option<List<B>> =
    when(xa) {
      is Nil -> Some(Nil)
      is Cons -> map2(f(xa.head), traverse(xa.tail, f)) { b, xb -> Cons(b, xb) }
    }
fun <A> sequenceT(xs: List<Option<A>>): Option<List<A>> = traverse(xs) { it }

fun main() {
  println(sequence(List.of(Some(1), Some(2), Some(3))))
  println(sequence(List.of(Some(1), Some(2), None)))

  println(sequenceT(List.of(Some(1), Some(2), Some(3))))
  println(sequenceT(List.of(Some(1), Some(2), None)))
}
