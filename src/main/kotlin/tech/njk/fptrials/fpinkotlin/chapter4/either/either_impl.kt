package tech.njk.fptrials.fpinkotlin.chapter4.either

import tech.njk.fptrials.fpinkotlin.chapter3.List
import tech.njk.fptrials.fpinkotlin.chapter3.Cons
import tech.njk.fptrials.fpinkotlin.chapter3.Nil


sealed class Either<out E, out A>

data class Left<out E>(val value: E) : Either<E, Nothing> ()
data class Right<out A>(val value: A) : Either<Nothing, A> ()

fun <A> TryToEither(a: () -> A): Either<Exception, A> =
    try {
      Right(a())
    } catch (e: Exception) {
      Left(e)
    }

fun <E, A, B> Either<E, A>.map(f: (A) -> B): Either<E, B> =
    when (this) {
      is Left -> this
      is Right -> Right(f(value))
    }

fun <E, A> Either<E, A>.orElse(f: () -> Either<E, A>): Either<E, A> =
    when (this) {
      is Left -> f()
      is Right -> this
    }

fun <E, A, B> Either<E, A>.flatMap( f: (A) -> Either<E, B>): Either<E, B> =
    when (this) {
      is Left -> this
      is Right -> f(value)
    }

fun <E, A, B, C> map2(ae: Either<E, A>, be: Either<E, B>, f: (A, B) -> C): Either<E, C> =
    ae.flatMap { a -> be.map { b -> f(a, b) } }

fun <E, A, B> traverse(xs: List<A>, f: (A) -> Either<E, B>): Either<E, List<B>> =
    when (xs) {
      is Nil -> Right(Nil)
      is Cons -> map2(f(xs.head), traverse(xs.tail, f)) { b, xb -> Cons(b, xb) }
    }

fun <E, A> sequence(es: List<Either<E, A>>): Either<E, List<A>> = traverse(es) { it }

//----------------------------------------------

data class Person(val name: Name, val age: Age)
data class Name(val value: String)
data class Age(val value: Int)

fun mkName(name: String): Either<String, Name> =
    if (name.isBlank()) Left("Name is empty.")
    else Right(Name(name))

fun mkAge(age: Int): Either<String, Age> =
    if (age < 0) Left("Age is out of range.")
    else Right(Age(age))

fun mkPerson(name: String, age: Int): Either<String, Person> =
    map2(mkName(name), mkAge(age)) { n, a -> Person(n, a) }

