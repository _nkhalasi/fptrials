package tech.njk.fptrials.datatypes.stackimperative

import arrow.core.*

typealias Stack = Option<Nel<String>>

fun pop(stack: Stack) = stack.fold(
    { None toT None },
    { Nel.fromList(it.tail) toT it.head.some() }
)

fun push(stack: Stack, s: String) = stack.fold(
    { Nel.of(s).some() toT Unit },
    { Nel(s, it.all).some() toT Unit }
)

fun stackOperations(stack: Stack): Tuple2<Stack, Option<String>> {
  val (s1, _) = push(stack, "a")
  val (s2, _) = pop(s1)
  return pop(s2)
}

fun main() {
  println(stackOperations(Nel.of("hello", "world", "!").some()))
  println(stackOperations(Nel.of("hello").some()))
}
