package tech.njk.fptrials.datatypes.user

import arrow.extension
import arrow.typeclasses.Eq
import arrow.core.extensions.*

data class User(val id: Int) {
  companion object
}

@extension
interface UserEq : Eq<User> {
  override fun User.eqv(b: User): Boolean = id == b.id
}


fun main() {
  String.eq().run { "1".eqv("2") }
  val naresh = User(1)
  val x = Eq.any().run { naresh.eqv(User(1)) }
  println(x)
}