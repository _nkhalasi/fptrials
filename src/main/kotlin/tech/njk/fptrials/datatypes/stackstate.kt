package tech.njk.fptrials.datatypes.stackstate

import arrow.core.*
import arrow.core.extensions.id.monad.monad
import arrow.core.extensions.either.monad.monad
import arrow.core.extensions.either.monadError.monadError
import arrow.mtl.State
import arrow.mtl.StateT
import arrow.mtl.extensions.fx

typealias Stack = Option<Nel<String>>

sealed class StackError {
  object StackEmpty : StackError()
}
typealias StackEmpty = StackError.StackEmpty

fun pop() = State<Stack, Option<String>> { stack ->
  stack.fold(
      { None toT None },
      { Nel.fromList(it.tail) toT it.head.some() }
  )
}

fun push(s: String) = State<Stack, Unit> { stack ->
  stack.fold(
      { Nel.of(s).some() toT Unit },
      { Nel(s, it.all).some() toT Unit }
  )
}

fun stackOperations() =
    State.fx<Stack, ForId, Option<String>>(Id.monad()) {
      val (a) = push("a")
      val (b) = pop()
      val (c) = pop()
      val (d) = pop()
      d
    }

fun popS(): StateT<Stack, EitherPartialOf<StackError>, String> =
    StateT<Stack, EitherPartialOf<StackError>, String> { stack ->
      stack.fold(
          { StackEmpty.left() },
          { (Nel.fromList(it.tail) toT it.head).right() }
      )
    }

fun pushS(s: String): StateT<Stack, EitherPartialOf<StackError>, Unit> =
    StateT<Stack, EitherPartialOf<StackError>, Unit> { stack ->
      stack.fold(
          { (Nel.just(s).some() toT Unit).right() },
          { (Nel(s, it.all).some() toT Unit).right() }
      )
    }

fun stackOperationsS(): StateT<Stack, EitherPartialOf<StackError>, String> =
    StateT.fx(Either.monadError<StackError>()) {
      val a = !pushS("a")
      val b = !popS()
      val c = !popS()
      val d = !popS()
      d
    }

fun main() {
  println(stackOperations().run { Nel.of("hello", "world", "!").some() })
  println(stackOperations().run { Nel.of("hello").some() })

  val v1 = stackOperationsS().run { Nel.of("hello", "world", "!").some() }
  println(v1)
  println(stackOperationsS().run { Nel.of("hello").some() })
}
