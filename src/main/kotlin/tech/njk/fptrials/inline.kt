package tech.njk.fptrials.inline

import org.slf4j.Logger
import org.slf4j.LoggerFactory


inline fun <reified T> membersOf() = T::class.members
inline fun <reified T> getClazz() : Class<T> = T::class.java
inline fun <reified T> Iterable<*>.filterIsInstance() = filter { it is T }
inline fun <reified T> logger(): Logger = LoggerFactory.getLogger(T::class.java)

data class User(val id: Int) {
  private val log = logger<User>()
}

fun main() {
  println(membersOf<StringBuilder>().joinToString("\n"))
  println(getClazz<User>())
  val set = setOf("1984", 2, 3, "Brave new world", 11)
  println(set.filterIsInstance<Int>())
}