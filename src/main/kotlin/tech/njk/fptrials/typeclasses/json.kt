package tech.njk.fptrials.typeclasses.json

import arrow.core.Either
import arrow.core.extensions.list.foldable.foldLeft
import arrow.core.left
import arrow.core.right
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonParser

typealias Result<T> = Either<Exception, T>

object Marshallers {
  fun <T : Any>convertToJson(m: T, ev: JsonMarshaller<T>): JsonElement = ev.run { m.toJson() }
  interface JsonMarshaller<T : Any> {
    fun T.toJson(): JsonElement
    companion object
  }
}

object Unmarshallers {
  fun <T : Any>convertFromJson(j: String, ev: JsonUnmarshaller<T>): Result<T> = ev.run { fromJson(j) }
  interface JsonUnmarshaller<T : Any> {
    fun fromJson(j: String): Result<T>
    companion object
  }
}

object Model {
  data class Product(val productId: String, val description: String) {
    companion object
  }
  data class OrderLine(val lineId: Int, val quantity: Int, val product: Product) {
    companion object
  }
  data class Order(val orderId: String, val orderLines: List<OrderLine>) {
    companion object
  }
}

object MarshallerInstances {
  val gson = Gson()

  val productMarshaller = object : Marshallers.JsonMarshaller<Model.Product> {
    override fun Model.Product.toJson(): JsonElement = gson.toJsonTree(this)
  }
  val orderLineMarshaller = object : Marshallers.JsonMarshaller<Model.OrderLine> {
    override fun Model.OrderLine.toJson(): JsonElement {
      val orderLine = gson.toJsonTree(this@toJson).asJsonObject
      orderLine.add("product", Marshallers.convertToJson(this@toJson.product, MarshallerInstances.productMarshaller))
      return orderLine
    }
  }

  val orderMarshaller = object : Marshallers.JsonMarshaller<Model.Order> {
    override fun Model.Order.toJson(): JsonElement {
      val orderLines = this@toJson.orderLines.map { Marshallers.convertToJson(it, orderLineMarshaller) }
      val withoutOrderLines = this@toJson.copy(orderLines = listOf())
      val json = gson.toJsonTree(withoutOrderLines)

      val acc = orderLines.foldLeft(JsonArray()) { res, el -> res.add(el); res }
      return json.asJsonObject.apply {
        remove("orderLines")
        add("customorderlines", acc)
      }
    }
  }
}

object UnmarshallerInstances {

  val gson = Gson()
  val parser = JsonParser()

  val productUnmarshaller = object : Unmarshallers.JsonUnmarshaller<Model.Product> {
    override fun fromJson(j: String): Result<Model.Product> = try {
      (gson.fromJson(j, Model.Product::class.java)).right()
    } catch (e: Exception){
      e.left()
    }
  }

  val orderLineUnmarshaller = object : Unmarshallers.JsonUnmarshaller<Model.OrderLine> {
    override fun fromJson(j: String): Result<Model.OrderLine> = try {
      // first use the productUnmarshaller to get a Result<Product>
      val jsonObject = parser.parse(j).asJsonObject
      val jsonProduct = jsonObject.get("product")
      val product = productUnmarshaller.fromJson(jsonProduct.toString())

      // if product is right, convert it to a product, else we get the error.
      product.map { p ->
        Model.OrderLine(jsonObject.get("lineId").asInt, jsonObject.get("quantity").asInt, p)
      }
    } catch (e: Exception){
      e.left()
    }
  }

  val orderUnmarshaller = object : Unmarshallers.JsonUnmarshaller<Model.Order> {
    override fun fromJson(j: String): Result<Model.Order> = try {

      val jsonObject = parser.parse(j).asJsonObject
      val jsonOrderLines = jsonObject.get("customorderlines").asJsonArray
      // convert using the correct unmarsahller
      val orderLines = jsonOrderLines.map { ol -> orderLineUnmarshaller.fromJson(ol.toString()) }

      // now we've got a List<Result<OrderLine>>. We'll reduce it to a Result<List<OrderLine>
      // so that we only continue of all succeed. Missing pattern matching and scala collections here.
      val rs = Either.Right(listOf<Model.OrderLine>())
      val orderLinesK = orderLines.foldLeft<Result<Model.OrderLine>, Result<List<Model.OrderLine>>>(rs) { res, ol ->
        when (res) {
          is Either.Left -> res
          is Either.Right -> when(ol) {
            is Either.Left -> ol                               // set the error
            is Either.Right -> Either.right(res.b.plus(ol.b))
          }
        }
      }

      // and finally return the unmarshalled object
      orderLinesK.map{ ols -> Model.Order(jsonObject.get("orderId").asString, ols)}
    } catch (e: Exception){
      Either.left(e)
    }
  }
}