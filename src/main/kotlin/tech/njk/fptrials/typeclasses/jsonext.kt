package tech.njk.fptrials.typeclasses.jsonext

import arrow.Kind
import arrow.core.Either
import arrow.core.extensions.list.foldable.foldLeft
import arrow.core.left
import arrow.core.right
import arrow.core.ListK
import arrow.core.extensions.listk.foldable.foldable
import arrow.extension
import arrow.typeclasses.Foldable
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonParser

typealias Result<T> = Either<Exception, T>

object Marshallers {
  fun <T : Any>convertToJson(m: T, ev: JsonMarshaller<T>): JsonElement = ev.run { m.toJson() }
  interface JsonMarshaller<T : Any> {
    fun T.toJson(): JsonElement
    companion object
  }
}

object Unmarshallers {
  fun <T : Any>convertFromJson(j: String, ev: JsonUnmarshaller<T>): Result<T> = ev.run { fromJson(j) }
  interface JsonUnmarshaller<T : Any> {
    fun fromJson(j: String): Result<T>
    companion object
  }
}

data class Product(val productId: String, val description: String) {
  companion object
}
data class OrderLine(val lineId: Int, val quantity: Int, val product: Product) {
  companion object
}
data class Order(val orderId: String, val orderLines: List<OrderLine>) {
  companion object
}


val gson = Gson()

/**
 * For the type of Kind<F, T> e.g ListK<T> we can automatically fold them using the provided typeclass. What
 * we need to know is evidence that it's foldable, and evidence on how to apply the marshaller to the embedded T
 * elements.
 */
fun <F, T: Any>foldableToJsonArrayMarshaller(fev: Foldable<F>, ev: Marshallers.JsonMarshaller<T>) =
    object: Marshallers.JsonMarshaller<Kind<F, T>> {
      override fun Kind<F, T>.toJson(): JsonElement = fev.run {
        foldLeft(JsonArray()) { b, a ->
          b.add(ev.run { a.toJson() })
          b
        }
      }
    }

@extension
interface ProductMarshallerInstance : Marshallers.JsonMarshaller<Product>  {
  override fun Product.toJson(): JsonElement = gson.toJsonTree(this)
}

@extension
interface OrderLineMarshallerInstance : Marshallers.JsonMarshaller<OrderLine> {
  override fun OrderLine.toJson(): JsonElement {
    val orderLine = gson.toJsonTree(this@toJson).asJsonObject
    orderLine.add("product", Marshallers.convertToJson(this@toJson.product, (object:ProductMarshallerInstance{})))
    return orderLine
  }
}

@extension
interface OrderMarshallerInstance : Marshallers.JsonMarshaller<Order> {
  override fun Order.toJson(): JsonElement {
    val orderLinesJson = foldableToJsonArrayMarshaller(ListK.foldable(), (object:OrderLineMarshallerInstance{})).run {
      ListK(orderLines).toJson()
    }
    return gson.toJsonTree(this@toJson.copy(orderLines = listOf())).asJsonObject.apply {
      remove("orderLines")
      add("customorderlines", orderLinesJson)
    }
//
//      val orderLines = this@toJson.orderLines.map { Marshallers.convertToJson(it, (object:OrderLineMarshallerInstance{})) }
//      val withoutOrderLines = this@toJson.copy(orderLines = listOf())
//      val json = gson.toJsonTree(withoutOrderLines)
//
//      val acc = orderLines.foldLeft(JsonArray()) { res, el -> res.add(el); res }
//      return json.asJsonObject.apply {
//        remove("orderLines")
//        add("customorderlines", acc)
//      }
  }
}

val parser = JsonParser()

@extension
interface ProductUnmarshallerInstance : Unmarshallers.JsonUnmarshaller<Product> {
  override fun fromJson(j: String): Result<Product> = try {
    (gson.fromJson(j, Product::class.java)).right()
  } catch (e: Exception){
    e.left()
  }
}

@extension
interface OrderLineUnmarshallerInstance : Unmarshallers.JsonUnmarshaller<OrderLine> {
  override fun fromJson(j: String) : Result<OrderLine> = try {
    // first use the productUnmarshaller to get a Result<Product>
    val jsonObject = parser.parse(j).asJsonObject
    val jsonProduct = jsonObject.get("product")
    val product = (object:ProductUnmarshallerInstance{}).fromJson(jsonProduct.toString())

    // if product is right, convert it to a product, else we get the error.
    product.map { p ->
      OrderLine(jsonObject.get("lineId").asInt, jsonObject.get("quantity").asInt, p)
    }
  } catch (e: Exception){
    e.left()
  }
}

@extension
interface OrderUnmarshallerInstance : Unmarshallers.JsonUnmarshaller<Order> {
  override fun fromJson(j: String): Result<Order> = try {

    val jsonObject = parser.parse(j).asJsonObject
    val jsonOrderLines = jsonObject.get("customorderlines").asJsonArray
    // convert using the correct unmarsahller
    val orderLines = jsonOrderLines.map { ol -> (object:OrderLineUnmarshallerInstance{}).fromJson(ol.toString()) }

    // now we've got a List<Result<OrderLine>>. We'll reduce it to a Result<List<OrderLine>
    // so that we only continue of all succeed. Missing pattern matching and scala collections here.
    val rs = Either.Right(listOf<OrderLine>())
    val orderLinesK = orderLines.foldLeft<Result<OrderLine>, Result<List<OrderLine>>>(rs) { res, ol ->
      when (res) {
        is Either.Left -> res
        is Either.Right -> when(ol) {
          is Either.Left -> ol                               // set the error
          is Either.Right -> Either.right(res.b.plus(ol.b))
        }
      }
    }

    // and finally return the unmarshalled object
    orderLinesK.map{ ols -> Order(jsonObject.get("orderId").asString, ols)}
  } catch (e: Exception){
    Either.left(e)
  }
}

