package tech.njk.fptrials.fpkt

import arrow.core.compose

fun main() {
  val compositeResult = (formatPrice compose getPrice)(books[1])
  println(compositeResult)
}