package tech.njk.fptrials.fpkt

data class Price(val value: Double, val currency: String = "$")

data class Book(
    val ISDN: String,
    val name: String,
    val pages: Int,
    val price: Price,
    val weight: Double,
    val year: Int,
    val author: String
)

val books = listOf(
    Book(
        "8850330731",
        "Android 3: Guida per lo sviluppatore (Italian Edition)",
        642,
        Price(40.06, "£"),
        1.8,
        2011,
        "Massimo Carli"
    ),
    Book(
        "8850333404",
        "Android 6: guida per lo sviluppatore (Italian Edition)",
        846,
        Price(39.26, "£"),
        2.1,
        2016,
        "Massimo Carli"
    )
)