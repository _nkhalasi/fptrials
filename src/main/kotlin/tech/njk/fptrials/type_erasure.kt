package tech.njk.fptrials

import java.math.BigDecimal

sealed class DocType(val str: String)
object Invoice: DocType("inv")
object PurchaseOrder: DocType("por")


data class Doc<DT: DocType> (
    val docNumber: String,
    val amount: BigDecimal,
    val docType: DT
)

typealias DocHandlerFn<DT> = (Doc<DT>) -> Unit
inline fun <reified DT: DocType> docHandler(doc: Doc<DT>, block: DocHandlerFn<DT>) =
    block(doc)

fun Invoice.docFactory(num: String, amt: BigDecimal) = Doc(num, amt, this)
fun PurchaseOrder.docFactory(num: String, amt: BigDecimal) = Doc(num, amt, this)
inline fun <reified DT: DocType> handleDocument(doc: Doc<DT>) = println(doc)

fun main() {
  val doc1 = Invoice.docFactory("1234", BigDecimal.valueOf(10.10))
  val doc2 = PurchaseOrder.docFactory("P1234", BigDecimal.valueOf(20.25))
  docHandler(doc1, ::handleDocument)
  docHandler(doc2, ::handleDocument)
}