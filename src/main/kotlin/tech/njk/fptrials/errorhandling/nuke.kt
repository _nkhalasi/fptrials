package tech.njk.fptrials.errorhandling

import arrow.core.*
import arrow.core.extensions.fx

/** model */
object Nuke
object Target
object Impacted

sealed class NukeException : Throwable() {
  object SystemOffline: NukeException()
  object RotationNeedsOil: NukeException()
  data class MissedByMeters(val meters : Int): NukeException()
  object PrepareError: NukeException()
}

typealias SystemOffline = NukeException.SystemOffline
typealias RotationNeedsOil = NukeException.RotationNeedsOil
typealias MissedByMeters = NukeException.MissedByMeters
typealias PrepareError = NukeException.PrepareError

class AttackTry {
  fun arm(): Try<Nuke> = Try { throw SystemOffline }
  fun aim(): Try<Target> = Try { throw RotationNeedsOil }
  fun launch(target: Target, nuke: Nuke): Try<Impacted> = Try { throw MissedByMeters(5) }

  fun attackTry(): Try<Impacted> =
      Try.fx {
        val (nuke) = arm()
        val (target) = aim()
        val (impact) = launch(target, nuke)
        impact
      }.fix()
}

class AttackEither {
//  fun prepare(): Either<PrepareError, Unit> = Unit.right()
  fun prepare(): Either<PrepareError, Unit> = PrepareError.left()
//  fun arm(): Either<SystemOffline, Nuke> = Right(Nuke)
  fun arm(): Either<SystemOffline, Nuke> = SystemOffline.left()
  fun aim(): Either<RotationNeedsOil, Target> = Right(Target)
  fun launch(target: Target, nuke: Nuke): Either<MissedByMeters, Impacted> = Left(MissedByMeters(5))

  fun attackEither(): Either<NukeException, Impacted> =
      Either.fx<NukeException, Impacted> {
        val x = ! prepare()
        //don't use the following tech.njk.fptrials.coroutines.coffeeshop.format for ignoring the returned right values
        // val (_) = prepare()
        val nuke = ! arm()
        val target = ! aim()
        val impact = ! launch(target, nuke)
        impact
      }
}

class AttackOption {
  fun arm(): Option<Nuke> {
    println("Nuke armed in AttackOption")
    return Nuke.some()
  }
  fun aim(): Option<Target> {
    println("Unable to aim target in AttackOption")
    return None
  }
  fun launch(target: Target, nuke: Nuke): Option<Impacted> = Some(Impacted)

  fun attackOption(): Option<Impacted> =
      Option.fx {
        val nuke = ! arm()
        val target = ! aim()
        val impact = ! launch(target, nuke)
        impact
      }
}

fun main() {
  println("---- Using TRY ----")
  println(AttackTry().attackTry())
  println("---- Using EITHER ----")
  println(AttackEither().attackEither())
  println("---- Using OPTION ----")
  println(AttackOption().attackOption())
}
