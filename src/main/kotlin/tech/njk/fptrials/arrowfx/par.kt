package tech.njk.fptrials.arrowfx.par

import arrow.fx.IO
import arrow.fx.extensions.fx
import arrow.unsafe
import kotlinx.coroutines.runBlocking

suspend fun threadName(): String =
    Thread.currentThread().name

suspend fun threadName(i: Int): String =
    "$i on ${Thread.currentThread().name}"

data class ThreadInfo(
    val threadA : String,
    val threadB: String
)

val parMapNprogram = IO.fx {
  val (threadA: String, threadB: String) = !dispatchers().default().parMapN(
      effect { threadName() },
      effect { threadName() },
      ::ThreadInfo
  )
  !effect { println(threadA) }
  !effect { println(threadB) }
}

val parTraverseProgram = IO.fx {
  val result: List<String> = !listOf(1,2,3).parTraverse{
    effect { threadName(it) }
  }
  !effect { println(result) }
}

val parSequenceProgram = IO.fx {
  val result: List<String> = !listOf(
      effect { threadName() },
      effect { threadName() },
      effect { threadName() }
  ).parSequence()

  !effect { println(result) }
}

fun main() { // The edge of our world
  unsafe { runBlocking { parMapNprogram } }
  println("---------------------------------")
  unsafe { runBlocking { parTraverseProgram } }
  println("---------------------------------")
  unsafe { runBlocking { parSequenceProgram } }
}