package tech.njk.fptrials.arrowfx.io

import arrow.fx.IO
import arrow.fx.extensions.fx
import arrow.fx.extensions.io.unsafeRun.runBlocking
import arrow.unsafe

suspend fun sayHello(): Unit =
    println("Hello World")

suspend fun sayGoodBye(): Unit =
    println("Good bye World!")

fun greetWithoutApplyingEffects(): IO<Unit> =
    IO.fx {
      val pureHello = effect { sayHello() }
      val pureGoodBye = effect { sayGoodBye() }
    }

fun greet(): IO<Unit> =
    IO.fx {
      !effect { sayHello() }
      !effect { sayGoodBye() }
    }

fun sayInIO(s: String): IO<Unit> =
    IO { println(s) }
fun greet2(): IO<Unit> =
    IO.fx {
      sayInIO("Hello from within IO block").bind()
    }

//suspend fun tech.njk.fptrials.coroutines.main(): Unit = IO.fx {
//  ! effect { sayHello() }
//}.suspended()

fun main() {
  println("--------W/O Effects--------------------------")
  print("1 ")
  println(greetWithoutApplyingEffects())
  print("2 ")
  println(greetWithoutApplyingEffects().attempt().unsafeRunSync())
  print("3 ")
  unsafe { runBlocking { greetWithoutApplyingEffects() } }
  println()

  println("--------With Effects--------------------------")
  print("1 ")
  println(greet())
  print("2 ")
  println(greet().attempt().unsafeRunSync())
  print("3 ")
  unsafe { runBlocking { greet() } }
  println()

  println("---------IO FX Composition-------------------------")
  print("1 ")
  println(greet2())
  print("2 ")
  println(greet2().attempt().unsafeRunSync())
  print("3 ")
  unsafe { runBlocking { greet2() } }
  println()
}