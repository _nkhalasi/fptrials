package tech.njk.fptrials.arrowfx.nomonads
//
//import arrow.unsafe
//import arrow.core.identity
//import arrow.core.toT
//import arrow.data.extensions.list.fx.fx
//import arrow.effects.extensions.io.fx.fx as iofx
//
//val result1 = fx(listOf(1, 2, 3), listOf(true, false), ::identity)
//val result2 = fx(listOf(true, false), listOf(1, 2), ::identity)
//
//val result3 = unsafe {
//  fx {
//    val (a) = listOf(1, 2)
//    val (b) = listOf(true, false)
//    a toT b
//  }
//}
//
//val result4 = unsafe {
//  fx {
//    val (b) = listOf(true, false)
//    listOf(1, 2) toT b
//  }
//}
//
//
//val result5 =
//    iofx {
//      val a = !effect { 1 }
//      val b = !effect { 2 }
//      a toT b
//    }
//
//val result6 =
//    iofx {
//      val b = !effect { 2 }
//      !effect { 1 } toT b
//    }
//
//fun tech.njk.fptrials.coroutines.main() {
//  println(result1)
//  println(result2)
//  println(result3)
//  println(result4)
//  println(result5)
//  println(result6)
//}