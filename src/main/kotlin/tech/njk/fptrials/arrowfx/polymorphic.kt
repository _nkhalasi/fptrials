package tech.njk.fptrials.arrowfx

import arrow.Kind
import arrow.fx.IO
import arrow.fx.extensions.io.concurrent.concurrent
import arrow.fx.extensions.io.unsafeRun.unsafeRun
import arrow.fx.typeclasses.Concurrent
import arrow.fx.typeclasses.UnsafeRun
import arrow.unsafe

/* a side effect */
val const = 1
suspend fun sideEffect(): Int {
  println(Thread.currentThread().name)
  return const
}

/* for all `F` that provide an `Fx` extension define a program function */
fun <F> Concurrent<F>.program(): Kind<F, Int> =
    fx.concurrent { !effect { sideEffect() } }

/* for all `F` that provide an `UnsafeRun` extension define a tech.njk.fptrials.coroutines.main function */
fun <F> UnsafeRun<F>.main(fx: Concurrent<F>): Int =
    unsafe { runBlocking { fx.program() } }

fun main() {
  IO.unsafeRun().main(IO.concurrent())
}
