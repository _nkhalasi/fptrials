package tech.njk.fptrials.arrowfx.async

import arrow.fx.IO
import arrow.fx.extensions.fx
import arrow.fx.extensions.io.unsafeRun.runBlocking
import arrow.unsafe
import kotlinx.coroutines.newSingleThreadContext

val contextA = newSingleThreadContext("A")

suspend fun printThreadName(): Unit =
    println("Executing in thread name=${Thread.currentThread().name}")

suspend fun threadName(): String =
    Thread.currentThread().name

val fibreProgram = IO.fx {
  val fiberA = !effect { threadName() }.fork(dispatchers().default())
  val fiberB = !effect { threadName() }.fork(dispatchers().default())
  val threadA = !fiberA.join()
  val threadB = !fiberB.join()
  !effect { println(threadA) }
  !effect { println(threadB) }
}

val program = IO.fx {
  continueOn(contextA)
  !effect { printThreadName() }
  continueOn(dispatchers().default())
  !effect { printThreadName() }
}

fun main() { // The edge of our world
  unsafe { runBlocking { fibreProgram } }
  println("-------------------------------")
  unsafe { runBlocking { program } }
}