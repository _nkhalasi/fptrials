import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  idea
  kotlin("jvm") version "1.3.71"
  kotlin("kapt") version "1.3.71"
}

group = "tech.njk"
version = "1.0-SNAPSHOT"
val arrowVersion = "0.10.5"
val ktorVersion = "1.1.3"
val spek2Version = "2.0.0"
val junitVersion = "5.2.0"

repositories {
  mavenCentral()
  mavenLocal()
  jcenter()
//  maven(url="https://oss.jfrog.org/artifactory/oss-snapshot-local/")
}

dependencies {
  runtimeOnly(kotlin("reflect"))
  implementation("ch.qos.logback", "logback-classic", "1.3.0-alpha4")
  implementation("io.arrow-kt", "arrow-core", arrowVersion)
  implementation("io.arrow-kt", "arrow-syntax", arrowVersion)
  implementation("io.arrow-kt", "arrow-fx", arrowVersion)
  implementation("io.arrow-kt", "arrow-fx-rx2", arrowVersion)
  implementation("io.arrow-kt", "arrow-mtl", arrowVersion)
  implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-core", "1.3.5")
  implementation("io.ktor", "ktor-gson", ktorVersion)

  kapt("io.arrow-kt", "arrow-meta", arrowVersion)

  testImplementation("org.junit.jupiter", "junit-jupiter-api", junitVersion)
  testImplementation("org.spekframework.spek2", "spek-dsl-jvm", spek2Version)
      .exclude("org.jetbrains.kotlin")
  testImplementation("org.amshove.kluent", "kluent", "1.47")
      .exclude("junit")
  testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", junitVersion)
  testRuntimeOnly("org.spekframework.spek2", "spek-runner-junit5", spek2Version)
      .exclude("org.junit.platform")
      .exclude("org.jetbrains.kotlin")
}

tasks.withType<Test> {
  useJUnitPlatform {
    includeEngines("spek2")
  }
}

tasks.withType<KotlinCompile> {
  kotlinOptions.jvmTarget = "1.8"
}

sourceSets {
  main {
    java {
      srcDir("build/generated/source/kaptKotlin/tech.njk.fptrials.coroutines.main/extension")
    }
  }
}
idea {
  module {
    generatedSourceDirs.add(
        file("build/generated/source/kaptKotlin/tech.njk.fptrials.coroutines.main/extension")
    )
  }
}